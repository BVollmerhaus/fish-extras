# Useful abbreviations for working with Git.
#
# Author: Benedikt Vollmerhaus <benedikt@vollmerhaus.org>
# License: MIT

if status --is-interactive
    abbr -a gcl 'git clone'
    abbr -a gf 'git fetch'
    abbr -a gp 'git push'
    abbr -a gpl 'git pull'

    abbr -a gm 'git merge'
    abbr -a gr 'git restore'
    abbr -a gcp 'git cherry-pick'

    # Staging
    abbr -a ga 'git add'
    abbr -a gu 'git restore --staged' # Unstage

    # Commit
    abbr -a gc 'git commit'
    abbr -a gcm 'git commit -m'
    abbr -a gca 'git commit --amend'

    # Branch
    abbr -a gb 'git branch'
    abbr -a gbd 'git branch -d'

    # Checkout
    abbr -a gco 'git checkout'
    abbr -a gcob 'git checkout -b'
    abbr -a gcom 'git checkout master'

    # Tag
    abbr -a gt 'git tag'
    abbr -a gta 'git tag -a'
    abbr -a gtd 'git tag -d'
    abbr -a gtl 'git tag -l'

    # Info
    abbr -a gd 'git diff'
    abbr -a gl 'git log'
    abbr -a gls 'git log --oneline'
    abbr -a gs 'git status'
    abbr -a gss 'git status --short'

    # Submodule
    abbr -a gsm 'git submodule'
    abbr -a gsmu 'git submodule update --remote --recursive'
end
