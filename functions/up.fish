# Synopsis:
#   up [NUM[!]]
#
# Description:
#   If called without args, go up to the current working directory's parent.
#
#   If NUM is supplied, go up that many directories from the CWD, stopping at
#   the user's home if it's on the way. This may be disabled by following NUM
#   with '!', in which case the home directory is passed through as usual.
#
# Author: Benedikt Vollmerhaus <benedikt@vollmerhaus.org>
# License: MIT

function up -a "num" -d "Go up a given number of directories from the CWD"
    if test -z $num
        cd ..
        return
    end

    if not string match -q -r '^\d+!?$' $num
        echo "Argument must be an integer (followed by '!' to not stop at ~)."
        return 1
    end

    set -l num (string trim --right --chars=! $num)
    set -l stop_at_home $status

    for i in (seq $num)
        if test $stop_at_home -eq 1 && test $i -gt 1 && test $PWD = $HOME
            echo "Reached home directory while going up, pausing."
            return
        end

        cd ..
    end
end
