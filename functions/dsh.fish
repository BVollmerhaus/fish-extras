# Synopsis:
#   dsh CONTAINER
#
# Description:
#   Open an interactive shell (/bin/sh) in the given Docker CONTAINER.
#
# Author: Benedikt Vollmerhaus <benedikt@vollmerhaus.org>
# License: MIT

function dsh -w "docker exec" -d "Open a shell in the given Docker container"
    docker exec -it $argv /bin/sh
end
