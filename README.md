# Benedikt's fish extras

Custom completions and perhaps useful functions for the
[fish shell](https://fishshell.com/).


## Installation

**Using [Fisher](https://github.com/jorgebucaran/fisher)**
```
fisher add gitlab.com/BVollmerhaus/fish-extras
```

**Using [Oh My Fish](https://github.com/oh-my-fish/oh-my-fish)**
```
omf install gitlab.com/BVollmerhaus/fish-extras
```


## Functions

### `up`

Go up a given number of directories from the current working directory.

**Usage:**
```
up [NUM[!]]
```

* If called without args, go up to the current working directory's parent.
* Otherwise, go up as many directories as specified by the supplied `NUM`.
* `NUM` may be followed by `!` to not stop when reaching the user's home.
